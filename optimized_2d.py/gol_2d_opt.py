import numpy as np
import copy
import matplotlib.pyplot as plt
import matplotlib.animation as animation


class Cell:
    def __init__(self, alive_neighbors: int, state: bool, y: int, x: int) -> None:
        if state not in [0, 1] or alive_neighbors < 0:
            print(f"ERROR: {x=}, {y=} has {state=} and {alive_neighbors=}")
        self.alive_neighbors = alive_neighbors
        self.state = state
        self.y = y
        self.x = x

    def __str__(self) -> str:
        return f"{self.y=} {self.x=}; {self.state=} {self.alive_neighbors=}"

    def __repr__(self) -> str:
        return f"{self.y=} {self.x=}; {self.state=} {self.alive_neighbors=}"

    @classmethod
    def update_state(self, new_state):
        self.state = new_state


def update_board_and_cell_list(frameNum, board: np.array, cell_list: list, ruleset: dict) -> tuple:
    print(f"generation")
    next_board = board.copy()
    next_cell_list = copy.deepcopy(cell_list)
    for y in range(board.shape[1]):
        for x in range(board.shape[0]):
            cell = cell_list[y][x]
            if cell.state == 0 and cell.alive_neighbors != 3:
                continue
            if cell.state == 1 and cell.alive_neighbors in [2, 3]:
                continue

            if cell.state == 1:
                next_board[y, x] = 0
                next_cell_list[y][x].state = 0
                next_cell_list = _update_adjacent_cells(
                    cords=(y, x), number=-1, next_cell_list=next_cell_list)
            else:
                next_board[y, x] = 1
                next_cell_list[y][x].state = 1
                next_cell_list = _update_adjacent_cells(
                    cords=(y, x), number=1, next_cell_list=next_cell_list)
    img.set_data(next_board)
    board[:] = next_board[:]
    cell_list[:] = next_cell_list[:]
    return img,


def _update_adjacent_cells(cords: tuple, number: int, next_cell_list: list) -> list:
    """TODO: Refactore!!!"""
    y, x = cords
    width = len(next_cell_list[0])-1
    height = len(next_cell_list)-1

    if y == 0 and x == 0:
        next_cell_list[y][x+1].alive_neighbors += number
        next_cell_list[y+1][x+1].alive_neighbors += number
        next_cell_list[y+1][x].alive_neighbors += number
    elif y == 0:
        if x == width:
            next_cell_list[y][x-1].alive_neighbors += number
            next_cell_list[y+1][x-1].alive_neighbors += number
            next_cell_list[y+1][x].alive_neighbors += number
        else:
            next_cell_list[y][x-1].alive_neighbors += number
            next_cell_list[y][x+1].alive_neighbors += number
            next_cell_list[y+1][x-1].alive_neighbors += number
            next_cell_list[y+1][x].alive_neighbors += number
            next_cell_list[y+1][x+1].alive_neighbors += number
    elif x == 0:
        if y == height:
            next_cell_list[y-1][x].alive_neighbors += number
            next_cell_list[y-1][x+1].alive_neighbors += number
            next_cell_list[y][x+1].alive_neighbors += number
        else:
            next_cell_list[y-1][x].alive_neighbors += number
            next_cell_list[y-1][x+1].alive_neighbors += number
            next_cell_list[y][x+1].alive_neighbors += number
            next_cell_list[y+1][x].alive_neighbors += number
            next_cell_list[y+1][x+1].alive_neighbors += number
    elif x == width and y == height:
        next_cell_list[y][x-1].alive_neighbors += number
        next_cell_list[y-1][x].alive_neighbors += number
        next_cell_list[y-1][x-1].alive_neighbors += number
    elif y == height:
        next_cell_list[y][x-1].alive_neighbors += number
        next_cell_list[y][x+1].alive_neighbors += number
        next_cell_list[y-1][x-1].alive_neighbors += number
        next_cell_list[y-1][x].alive_neighbors += number
        next_cell_list[y-1][x+1].alive_neighbors += number
    elif x == width:
        next_cell_list[y-1][x].alive_neighbors += number
        next_cell_list[y-1][x-1].alive_neighbors += number
        next_cell_list[y][x-1].alive_neighbors += number
        next_cell_list[y+1][x].alive_neighbors += number
        next_cell_list[y+1][x-1].alive_neighbors += number
    else:
        next_cell_list[y-1][x-1].alive_neighbors += number
        next_cell_list[y-1][x].alive_neighbors += number
        next_cell_list[y-1][x+1].alive_neighbors += number
        next_cell_list[y][x-1].alive_neighbors += number
        next_cell_list[y][x+1].alive_neighbors += number
        next_cell_list[y+1][x-1].alive_neighbors += number
        next_cell_list[y+1][x].alive_neighbors += number
        next_cell_list[y+1][x+1].alive_neighbors += number
    return next_cell_list


def _init_board(matrix_size: tuple) -> list:
    board = _add_random_data(matrix_size)
    cell_list = board.copy().tolist()
    for y in range(board.shape[1]):
        for x in range(board.shape[0]):
            cell_list[y][x] = Cell(
                alive_neighbors=_get_alive_neighbor_count(
                    board=board, cords=(y, x)),
                state=board[y, x],
                y=y,
                x=x)
    return board, cell_list


def _get_neighborhood(board: list, cords: tuple) -> np.array:
    y, x = cords
    if y == 0 and x == 0:
        board_slice = board[y:y+2, x:x+2]
    elif y == 0:
        board_slice = board[y:y+2, x-1:x+2]
    elif x == 0:
        board_slice = board[y-1:y+2, x:x+2]
    else:
        board_slice = board[y-1:y+2, x-1:x+2]
    return board_slice


def _get_alive_neighbor_count(board: list, cords: tuple):
    y, x = cords
    non_zero_count = np.count_nonzero(
        _get_neighborhood(board=board, cords=cords))
    if board[y, x] == 1:
        non_zero_count -= 1  # subtract ifself
    return non_zero_count


def _add_random_data(matrix_size: tuple) -> np.array:
    return np.random.choice([0, 1], size=matrix_size[0]*matrix_size[1]).reshape(matrix_size)


if __name__ == "__main__":
    ruleset = {
        'con_die_upper': 4,
        'con_die_lower': 1,
        'con_revive': 3
    }
    board, cell_list = _init_board((500, 500))
    fig, ax = plt.subplots()
    img = ax.imshow(board, interpolation='nearest')
    ani = animation.FuncAnimation(fig, update_board_and_cell_list, fargs=(board, cell_list, ruleset),
                                  frames = 60,
                                  interval=100,
                                  save_count=50)
    plt.show()
