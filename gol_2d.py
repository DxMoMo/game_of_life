from typing import Tuple
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation


def update_board(frameNum, board: np.array, con_die_upper: int, con_die_lower: int, con_revive: int) -> np.array:
    new_board = board.copy()
    for y in range(board.shape[0]):
        for x in range(board.shape[1]):
    
            orig_value = board[y, x]
            non_zero_count = _get_non_zero_count(board, y, x)
            if orig_value == 0:
                if non_zero_count == con_revive:
                    new_board[y, x] = 1
            else:
                if non_zero_count >= con_die_upper or non_zero_count <= con_die_lower:
                    new_board[y, x] = 0
    img.set_data(new_board)
    board[:] = new_board[:]
    return img,


def _get_non_zero_count(board: np.array, y: int, x: int) -> np.array:
    orig_value = board[y, x]
    if y == 0 and x == 0:
        board_slice = board[y:y+2, x:x+2]
    elif y == 0:
        board_slice = board[y:y+2, x-1:x+2]
    elif x == 0:
        board_slice = board[y-1:y+2, x:x+2]
    else:
        board_slice = board[y-1:y+2, x-1:x+2]
    non_zero_count = np.count_nonzero(board_slice)  
    if orig_value == 1:
        non_zero_count -= 1  # subtract ifself
    return non_zero_count



def init_initital_board(matrix_size: Tuple[int, int]) -> np.array:
    board = _add_random_data(matrix_size)
    return board


def _add_random_data(matrix_size: Tuple[int, int]) -> np.array:
    return np.random.randint(5, size=matrix_size)


def _add_glider_top_left(matrix_size: Tuple[int, int]) -> np.array:
    board = np.zeros(matrix_size, dtype=int)
    board[0, 1] = 1
    board[1, 2] = 1
    board[2, 0] = 1
    board[2, 1] = 1
    board[2, 2] = 1
    return board


if __name__ == "__main__":
    # set up animation
    fig, ax = plt.subplots()
    board = init_initital_board((350, 350))
    img = ax.imshow(board, interpolation='nearest')
    ani = animation.FuncAnimation(fig, update_board, fargs=(board, 4, 1, 3),
                                  frames = 60,
                                  interval=100,
                                  save_count=50)
    plt.show()


